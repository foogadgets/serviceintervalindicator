#compiling settings
PROCESSOR	= 12F1840
CC		= xc8
PROJ		= serviceintervalindicator
PROJ_DIR	= .
GEN_DIR		= /tmp/$(PROJ)
BUILD_DIR	= ${GEN_DIR}/build
BIN_DIR		= ${GEN_DIR}/bin
OBJ_DIR		= ${GEN_DIR}/obj
CC_INCLUDE	= -I/Applications/microchip/xc8/v1.34/include/
CC_FLAGS	= -P -N255 --warn=0 --addrqual=ignore --mode=pro# --echo
OPTIMISATION	= --opt=default,+asm,-asmfile,+speed,-space,-debug,9
OPTIMISATION   += --runtime=+plib
OUTPUT		= --output=intel --outdir=${BUILD_DIR} --objdir=${OBJ_DIR}
OUTFILE		= $(PROJ).hex
MKDIRS		= mkdir -p $(BUILD_DIR) $(BIN_DIR) $(OBJ_DIR)

CHIP		= --chip=${PROCESSOR}
ALL_C		= ${PROJ_DIR}/$(PROJ).c

#programming settings
PROGRAM		= /Applications/pk2cmd/pk2cmd
DEVICE		= -pPIC${PROCESSOR}

#datasheet stuff
READER		= okular
PREFIX		= PIC
SUFFIX		= .pdf
DIR		= ~/datasheets/IC\'s/Microchip/PIC/


.PHONY : all clean clean_all prog program restore erase on off info help
.DEFAULT : all
${ALL_C} all:
	${MKDIRS}
	${CC} ${CC_FLAGS} ${CHIP} ${OPTIMISATION} ${ALL_C} ${OUTPUT}
	cp ${BUILD_DIR}/${OUTFILE} ${BIN_DIR}/${OUTFILE}
clean:
	rm -rf ${GEN_DIR}
prog program:
	${PROGRAM} -B/Applications/pk2cmd/ ${DEVICE} -f${BIN_DIR}/${OUTFILE} -m -j
on:
	${PROGRAM} -B/Applications/pk2cmd/ ${DEVICE} -t
off:
	${PROGRAM} -B/Applications/pk2cmd/ ${DEVICE} -w
dump:
	@$(PROGRAM) -B/Applications/pk2cmd/ ${DEVICE} -GE0-5
erase:
	${PROGRAM} -B/Applications/pk2cmd/ ${DEVICE} -e -j
info:
	${PROGRAM} -B/Applications/pk2cmd/ ${DEVICE} -I
datasheet:
	${READER} ${DIR}/${PREFIX}${PROCESSOR}${SUFFIX}

