# README #


### What is this repository for? ###

Keeps track of the service intervals on tractors etc.
One LED that blinks when the service interval time has been reached.
One button that resets the timer. Timer is reset when button is held at power on.

### How do I get set up? ###

* Install XC8 compiler
* Clone this repository
* Set up your Pickit2/Pickit3 so that it is ready to program the PIC12F1840
* Adjust the Service Interval Time in the code by adjusting the INSERVICETIME parameter. Default is 10 hours interval
* At the command prompt, type: make all; make prog


Code compiles successfully. It has been lab tested, but not i a real vehicle.
