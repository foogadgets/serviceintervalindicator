/*
 * File:   serviceindicator.c
 * Author: Niclas Forslund
 * E-mail: foogadgets@gmail.com
 * Created on October 17, 2017, 20:33
 *
 * LED connects from RA2 (Pin5)  through a resistor (1k) to GND.
 * Button connects between RA3 (Pin4) and GND.
 * Adjust the INSERVICETIME parameter to the amount of hours before
 * service indication. The service indication is a LED that blinks
 * at 6Hz.
 * 
 * After service the Service indicator is reset by holding the button
 * pressed during power on (i.e. ignition on).
 * Counter can only be reset when Service time has been reached.
 *
 */

#include <xc.h>
#include <stdint.h>
#include "defines.h"


// In service time: Number of hours in service before service indication
#define INSERVICETIME	10




/* NO CHANGES NEEDED BELOW THIS COMMENT */



// Set EEPROM data to a known start value (MSB)
__EEPROM_DATA(	0, 0,	// First 16 bit version of counter data
				0, 0,	// Second 16 bit version of counter data
				0, 0,	// Third 16 bit version of counter data
				0, 0);	// Not used

// Base address: EEPROM base address. Six consecutive addresses is used
// to be able to detect if the eeprom writing was interrupted by a power off
#define BASEADDRESS	0

// Milliseconds to relax after button press to let signal settle
#define DEBOUNCE 20

#define LED	2	// Output
#define BUTTON	3	// Input


// Convert in-service-time to number of 256s intervals
// WDT set to 256s timeouts. => 4m16s. 1h => 14.0625 4m16s-intervals
uint16_t serviceLevel = INSERVICETIME*14;
uint16_t counterValue = 0ULL;


void Setup(void);
void checkButton(void);
void blinkLED(void);
void writeToEeprom(uint8_t, uint16_t);
uint16_t readFromEeprom(uint8_t);



void main()
{
	Setup();
	counterValue = readFromEeprom(BASEADDRESS);
	if(counterValue == serviceLevel) checkButton(); // Only possible if in service mode

	while(counterValue < serviceLevel)	// Every round takes 256 seconds
	{
		SLEEP();
		NOP();
		counterValue += 1;
		writeToEeprom(BASEADDRESS, counterValue);
	}

	// Service needed. Start LED indicator.
	blinkLED();
}




// Read current stored EEPROM-value and return it as a 16 bit unsigned Int
// The counter value is saved in six consecutive addresses to be able
// to conclude if the EEPROM write was interrupted.
uint16_t readFromEeprom(uint8_t address) {
	uint16_t val=0ULL;
	uint16_t val0=0ULL;
	uint16_t val1=0ULL;

	while(WR) CLRWDT();	// Make sure no writing is ongoing before a read
	val = eeprom_read(address+0)<<8;
	val += eeprom_read(address+1);
	val0 = eeprom_read(address+2)<<8;
	val0 += eeprom_read(address+3);
	val1 = eeprom_read(address+4)<<8;
	val1 += eeprom_read(address+5);

	if (val1 == val0) val = val0;
	return val;
}

// Writes same value to six consecutive EEPROM addresses
void writeToEeprom(uint8_t address, uint16_t value) {
	WDTCONbits.SWDTEN = 0;	// Disable Watchdog
	eeprom_write(address+0, (value>>8)&0xff);
	eeprom_write(address+1, value&0xff);
	eeprom_write(address+2, (value>>8)&0xff);
	eeprom_write(address+3, value&0xff);
	eeprom_write(address+4, (value>>8)&0xff);
	eeprom_write(address+5, value&0xff);
	WDTCONbits.SWDTEN = 1;	// Enable Watchdog
}


// Function is called during power on to check if the counter should
// be restarted from 0. Simple debounce. Nothing fancy is needed.
void checkButton(void) {
	if (0 == (PORTA&(1<<BUTTON))) {
		__delay_ms(DEBOUNCE);
		if (0 == (PORTA&(1<<BUTTON))) {
			writeToEeprom(BASEADDRESS, 0x0000);
		}
	}
}


// If the serviceLevel is reached, the LED start to blink at
// about 6Hz until the counter is reset.
void blinkLED(void)
{
	WDTCONbits.SWDTEN = 0;	// Disable Watchdog
	while (1) {
		LATA |= (1<<LED);
		__delay_ms(80);
		LATA &= ~(1<<LED);
		__delay_ms(80);
	}
}



void Setup(void) {
	OSCCONbits.SPLLEN	= 0b0;		// No PLL
	OSCCONbits.SCS		= 0b10;		// Use internal oscillator block
	OSCCONbits.IRCF		= 0b1111;	// Set internal clock to 16MHz

	CM1CON0		= 0x00;		// Disable comparator
	FVRCON		= 0x00;		// Disable Voltage ref.
	ADCON0bits.ADON	= 0x00;	// Disable ADC

	ANSELA	= 0x00;			// All pins set to Digital
	TRISA	= (1<<BUTTON);	// Only one input
	WPUA	= (1<<BUTTON);	// Avoiding external pull-up resistor
	PORTA	= 0x00;

	OPTION_REGbits.nWPUEN = 0;	// Enable weak pull-ups

	VREGCON		= 0x3;	// Deep sleep enabled. Not really needed.

	WDTCONbits.WDTPS = 0b10010;	// 256s Watchdog intervals
	//WDTCONbits.WDTPS = 0b01100;	// TEST 4s intervals
	WDTCONbits.SWDTEN = 1;		// Enable Watchdog trigger
}

