#ifndef DEFINES_H
#define	DEFINES_H

/* Config Register CONFIGL @ 0x8007 */
#pragma config CPD = OFF    // Data memory code protection is disabled
#pragma config BOREN = ON   // Brown-out Reset enabled
#pragma config IESO = OFF   // Internal/External Switchover mode is disabled
#pragma config FOSC = INTOSC // INTOSC oscillator: I/O function on CLKIN pin
#pragma config FCMEN = OFF  // Fail-Safe Clock Monitor is disabled
#pragma config MCLRE = OFF  // MCLR/VPP pin function is MCLR
#pragma config WDTE = 1  // WDT controlled in software
#pragma config CP = OFF     // Program memory code protection is disabled
#pragma config PWRTE = OFF  // PWRT enabled
#pragma config CLKOUTEN = OFF // CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin

/* Config Register CONFIG2 @ 0x8008 */
#pragma config PLLEN = OFF  // 4x PLL enabled
#pragma config WRT = OFF    // Write protection off
#pragma config STVREN = OFF // Stack Overflow or Underflow will not cause a Reset
#pragma config BORV = HI    // Brown-out Reset Voltage (Vbor), high trip point selected.
#pragma config LVP = OFF    // High-voltage on MCLR/VPP must be used for programming

// Preprocessor define for __delay_ms() and __delay_us()
#define _XTAL_FREQ 16000000

#endif	/* DEFINES_H */

